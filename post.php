<?php

// Set the name of the text file where statuses will be stored
$file = 'statuses.txt';

if (!file_exists($file)) {
    touch($file);
}
// Check if the form has been submitted
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  
  // Get the name and status text from the form
  $name = $_POST['name'];
  $status = $_POST['status'];

  // Convert any special characters in the name and status text to HTML entities
  $name = htmlspecialchars($name, ENT_QUOTES, 'UTF-8');
  $status = htmlspecialchars($status, ENT_QUOTES, 'UTF-8');

  // Append the name and status text to the text file
  $current = file_get_contents($file);
  $current .= "$name:\n&lcub;$status&rcub;\n";
  file_put_contents($file, $current);
  
  // Reload the page to show the updated statuses
  header('Location: ' . $_SERVER['PHP_SELF']);
  exit;
}

// Read the contents of the text file
$statuses = file_get_contents($file);
?>

<!DOCTYPE html>
<html>
<head>
  <title>PHPost</title>
</head>
<body>
  <h1>PHPost</h1>
  <form method="post">
    <label for="name">Name:</label><br>
    <input type="text" id="name" name="name"><br><br>
    <label for="status">Post:</label><br>
    <textarea id="status" name="status"></textarea><br><br>
    <input type="submit" value="Post">
  </form>
  
  <hr>
  
  <h2>Posts</h2>
  <pre><?php echo $statuses; ?></pre>
</body>
</html>
